package com.example.authentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase


import kotlinx.android.synthetic.main.activity_sign_up.*



class SignUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()

    }

    private fun init() {
        auth = Firebase.auth
        registrationButton.setOnClickListener {
            signUp()
        }
    }

    private fun signUp() {
        val email: String = emailEditText.text.toString()
        val password: String = passwordEditText.text.toString()
        val repeatPassword: String = repeatPasswordEditText.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty() && repeatPassword.isNotEmpty()) {
            if (password == repeatPassword) {
                progressBar.visibility = View.VISIBLE
                deleteClick(true)
                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        progressBar.visibility = View.GONE
                        deleteClick(false)
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            d("signUp", "createUserWithEmail:success")
                            Toast.makeText(
                                this, "SignUp is Success!",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            d("signUp", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(
                                this, "${task.exception}",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            } else {
                Toast.makeText(this, "Passwords Do not Match!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun deleteClick(isStarted: Boolean) {
        registrationButton.isClickable = !isStarted
    }
}